import React from "react";
import "./Biodata.css";
import Label from "../components/Label";
import Input from "../components/Input";
import Button from "../components/Button";
import Box from "../components/Box";
import { Link } from "react-router-dom";

export default function Biodata() {
  return (
    <body>
      <div className="containerbio">
        <div className="left">
          <Link to={"/game"} className="click">
            <h1>PLAYER VS COMPUTER</h1>
          </Link>
          <div className="line"></div>
          <div className="center">
            <h2>CREATE NEW ROOM</h2>
          </div>
          <div className="posisi">
            <Link to={"/creategame"} className="posisi">
              <div className="box">
                <Box />
              </div>
              <div className="box">
                <Box />
              </div>
              <div className="box">
                <Box />
              </div>
            </Link>
          </div>
        </div>
        <div className="right">
          <form className="formstyle">
            <h1>PROFILE</h1>
            <div className="stylelabel">
              <Label for="Nama Lengkap :" />
              <Input type="text" />
            </div>
            <div className="stylelabel">
              <Label for="Number Phone " />
              <Input type="text" />
            </div>
            <div className="stylelabel">
              <Label for="Address :" />
              <Input type="text" />
            </div>
            <div className="stylelabel">
              <Label for="Hobbies :" />
              <Input type="text" />
            </div>
            <div className="stylelabel">
              <Label for="Motto :" />
              <Input type="text" />
            </div>
            <Link to={"/history"}>
              <Button title="History Game" />
            </Link>
          </form>
        </div>
      </div>
    </body>
  );
}
