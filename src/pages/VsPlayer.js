import React from 'react'
import "./VsPlayer.css"

export default function VsPlayer() {
  return (
    <body>
      <div className="containerGame">
        <div className="bawah">
          <h1>Player 1</h1>
          <img src="./assets/image/batu.png" alt="batu" className="gambarp" />
          <img
            src="./assets/image/kertas.png"
            alt="kertas"
            className="gambarp"
          />
          <img
            src="./assets/image/gunting.png"
            alt="gunting"
            className="gambarp"
          />
        </div>
        <div>
          <h1>VS</h1>
          <img
            src="./assets/image/refresh.png"
            alt="refresh"
            className="gambarp"
          />
        </div>
        <div className="bawah">
          <h1>Player 2</h1>
          <img src="./assets/image/batu.png" alt="batu" className="gambarp" />
          <img
            src="./assets/image/kertas.png"
            alt="kertas"
            className="gambarp"
          />
          <img
            src="./assets/image/gunting.png"
            alt="gunting"
            className="gambarp"
          />
        </div>
      </div>
    </body>
  )
}
