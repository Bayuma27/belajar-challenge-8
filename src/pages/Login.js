import React from "react";
import "./Login.css";
import Button from "../components/Button";
import Label from "../components/Label";
import Input from "../components/Input";

export default function Login() {
  return (
    <div id="body">
      <div className="container">
        <h1>Login</h1>
        <Label for="Username/Email" />
        <Input type="text" />

        <Label for="Password" />
        <Input type="password" />

        <Button title="Login" />

        <p>Forgot your password?</p>
      </div>
    </div>
  );
}
