import React from "react";
import "./Register.css";
import Button from "../components/Button";
import Label from "../components/Label";
import Input from "../components/Input";

export default function Register() {
  return (
    <div id="body">
      <div className="container">
        <h1>Registrasi</h1>
        <Label for="Username" />
        <Input type="text" />

        <Label for="Email" />
        <Input type="email" />

        <Label for="Password" />
        <Input type="password" />

        <Label for="Confirm Password" />
        <Input type="password" />

        <Button title="Registrasi" />
      </div>
    </div>
  );
}
