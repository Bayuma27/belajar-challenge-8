import React from "react";
import Input from "../components/Input";
import "./CreateGame.css";
import Button from "../components/Button";
import { Link } from "react-router-dom";

export default function CreateGame() {
  return (
    <body>
      <div className="containerCreateGame">
        <h1 className="batasatas">Tuliskan Nama Room</h1>
        <div>
          <Input type="text" />
        </div>
        <h2>Tentukan Pilihanmu!</h2>
        <div className="gambarsamping">
          <img src="./assets/image/batu.png" alt="batu" className="gambarc" />
          <img
            src="./assets/image/kertas.png"
            alt="kertas"
            className="gambarc"
          />
          <img
            src="./assets/image/gunting.png"
            alt="gunting"
            className="gambarc"
          />
        </div>
        <div className="ukuranbutton">
          <Link to={"/player"}>
            <Button title="CREATE GAME" />
          </Link>
        </div>
      </div>
    </body>
  );
}
