import React from "react";
import "./Navbar.css";
import { Link, Outlet } from "react-router-dom";

export default function Navbar() {
  return (
    <body>
      <div className="navbar">
        <Link to={"/bio"} className="nounderline">
          <span>Dashboard</span>
        </Link>
        <div className="navbar-right">
          <Link to={"/login"} className="nounderline">
            <span>Login</span>
          </Link>
          <Link to={"/registrasi"} className="nounderline"> 
            <span>Registrasi</span>
          </Link>
        </div>
      </div>
      <Outlet />
    </body>
  );
}
