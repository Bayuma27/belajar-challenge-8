import React from "react";
import "./Game.css";

// karena penambahan komponen untuk gambar masih error dan belum bisa maka menggunakan manual :)

export default function Game() {
  return (
    <body>
      <div className="containerGame">
        <div className="bawah">
          <h1>Player</h1>
          <img src="./assets/image/batu.png" alt="batu" className="gambar" />
          <img
            src="./assets/image/kertas.png"
            alt="kertas"
            className="gambar"
          />
          <img
            src="./assets/image/gunting.png"
            alt="gunting"
            className="gambar"
          />
        </div>
        <div>
          <h1>VS</h1>
          <img
            src="./assets/image/refresh.png"
            alt="refresh"
            className="gambar"
          />
        </div>
        <div className="bawah">
          <h1>COM</h1>
          <img src="./assets/image/batu.png" alt="batu" className="gambar2" />
          <img
            src="./assets/image/kertas.png"
            alt="kertas"
            className="gambar2"
          />
          <img
            src="./assets/image/gunting.png"
            alt="gunting"
            className="gambar2"
          />
        </div>
      </div>
    </body>
  );
}
