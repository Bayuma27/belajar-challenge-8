import React, { useState } from "react";
import "./History.css";

export default function History() {
  const [posts] = useState([
    {
      title: "Game One",
      time: "20.00 WIB",
      status: "LOSE",
    },
    {
      title: "Game Two",
      time: "21.00 WIB",
      status: "WIN",
    },
    {
      title: "Player VS COM",
      time: "22.00 WIB",
      status: "WIN",
    },
    {
      title: "Player VS COM",
      time: "23.00 WIB",
      status: "LOSE",
    },
  ]);
  return (
    <div className="history">
      <div className="batas">
        <h1>History Game</h1>
      </div>
      <div className="historybox">
        <table>
          <thead>
            <tr>
              <th>TITLE</th>
              <th>TIME</th>
              <th>STATUS</th>
            </tr>
          </thead>
          <tbody>
            {posts.map((post, index) => (
              <tr key={index}>
                <td>{post.title}</td>
                <td>{post.time}</td>
                <td>{post.status}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}
