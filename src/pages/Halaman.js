import React from "react";
import "./Halaman.css";
import Button from "../components/Button";
import { Link } from "react-router-dom";

export default function Halaman() {
  return (
    <div className="halaman">
      <div className="halamanbox">
        <h3>Tekan Tombol di Bawah untuk Memulai Game</h3>
        <Link to="/login">
          <Button title="PLAY GAME" />
        </Link>
      </div>
    </div>
  );
}
