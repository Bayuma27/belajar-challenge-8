import { Route, Routes } from "react-router-dom";
import "./App.css";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Biodata from "./pages/Biodata";
import Game from "./pages/Game";
import Navbar from "./pages/Navbar";
import History from "./pages/History";
import CreateGame from "./pages/CreateGame";
import Halaman from "./pages/Halaman";
import VsPlayer from "./pages/VsPlayer";

const App = () => {
  return (
    <Routes>
      <Route path="" element={<Halaman/>} />
      <Route path="/" element={<Navbar/>}>
      <Route path="/login" element={<Login />} />
      <Route path="/registrasi" element={<Register />} />
      <Route path="/bio" element={<Biodata />} />
      <Route path="/creategame" element={<CreateGame />} />
      <Route path="/player" element={<VsPlayer />} />
      <Route path="/game" element={<Game />} />
      <Route path="/history" element={<History/>} />
      </Route>
    </Routes>
  );
};

export default App;
