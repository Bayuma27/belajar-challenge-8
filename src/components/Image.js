const Image = (props) => {
    const { src, id, onCLick} = props;
    return (
        <img
        src={src}
        id={id}
        onClick={onCLick}/>
    )
}

export default Image;